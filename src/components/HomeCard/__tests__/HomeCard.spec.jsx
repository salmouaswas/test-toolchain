import React from 'react';
import { shallow } from 'enzyme';
import HomeCard from '../index';

const validProps = {
    cards: {
        cardLinkUrl: "/get-upload-token",
        cardImage: Image,
        cardTitle: "title",
        cardDescription: "description"
    }
};
describe('HomeCard/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HomeCard {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('finds image', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HomeCard {...validProps} />);
        expect(wrapper.find('cardImage'));
    });
});