import React from "react";
import { Link } from "@reach/router";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import log from "../../assets/images/log.svg";
import offboarding from "../../assets/images/offboarding.svg";
import token from "../../assets/images/token.svg";
import "./style.scss";

const cardImage = (imageName) => {
  switch (imageName) {
    case "token":
      return token;
    case "log":
      return log;
    case "offboarding":
      return offboarding;
    default:
      return log;
  }
};

const rootClass = "home-card";

function HomeCard(props) {
  const { cards } = props;
  return (
    <Card className={rootClass}>
      <Link to={cards.cardLinkUrl} className={`${rootClass}__link`}>
        <CardContent className={`${rootClass}__content`}>
          <div className={`${rootClass}__card`}>
            <div className={`${rootClass}__flex`}>
              <img
                src={cardImage(cards.cardImage)}
                alt="card logo"
                className={`${rootClass}__image`}
              />
            </div>
            <div className={`${rootClass}__flex`}>
              <Typography gutterBottom variant="h6" component="h2">
                {cards.cardTitle}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {cards.cardDescription}
              </Typography>
              </div>
            </div>
        </CardContent>
      </Link>
    </Card>
  );
}

export default HomeCard;
