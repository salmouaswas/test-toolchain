import React from 'react';
import { mount } from 'enzyme';
import Header from '../index';

describe('<Header/>', () => {
    global.window = { location: { pathname: null } };
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = mount(<Header />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('has image', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = mount(<Header />);
        expect(wrapper.find('home'));
    });
    it('renavigates', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = mount(<Header />);
        wrapper.find('button').simulate('click');
        expect(global.window.location.pathname).toEqual('/');
    });
});