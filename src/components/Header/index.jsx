import { navigate } from "@reach/router";
import React from "react";
import home from "../../assets/images/home.svg";
import "./styles.scss";

const Header = () => {
  const rootClass = "header";
  const returnHome = () => {
    navigate("/");
  };
  return (
    <div className={rootClass}>
      <div className={`${rootClass}__buttonWrapper`}>
      <button
        onClick={returnHome}
        type="button"
        className={`${rootClass}__button`}
      >
        <img src={home} alt="home icon" className={`${rootClass}__button__image`}/>
        <p className={`${rootClass}__button__text`}>Home</p>
      </button>
      </div>
    </div>
  );
};
export default Header;
