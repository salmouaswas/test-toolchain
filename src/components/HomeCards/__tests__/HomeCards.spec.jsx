import React from 'react';
import { shallow } from 'enzyme';
import HomeCards from '../index';

const validProps = {
    cards: [{
        cardLinkUrl: "/get-upload-token",
        cardImage: Image,
        cardTitle: "title",
        cardDescription: "description"
    }]
};
describe('HomeCards/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HomeCards {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
});