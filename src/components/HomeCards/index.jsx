import React from "react";
import HomeCard from "../HomeCard";
import "./style.scss";

const rootClass = "home-cards";

export default function HomeCards(props) {
  const { cards } = props;
  return (
    <div className={`${rootClass}`}>
      {cards.map((card, index) => (
        <div key={card.cardTitle} index={index}>
          <HomeCard cards={card} />
        </div>
      ))}
    </div>
  );
}
