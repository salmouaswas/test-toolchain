/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { shallow } from 'enzyme';
import LogResultsPage from '../index';

const validProps = {
    searchResponse: [{
        CENTRAL_USER_ID: 1313134,
        PERIPHERAL_USER_ID: 1313212,
        CLOSE_CONTACT: "5195641231",
        CONTACT_DATE: "2020-04-20",
        TOTAL_ENCOUNTERS: 4,
        LAST_ENCOUNTER: "2020-04-20 12:28:38.000000",
        FIRST_ENCOUNTER: "2020-04-20 10:28:13.000000",
        CONTACT_TIME_SEC: 7225,
        TRANSFERRED_TO_CDM: false
    }],
    parameters: jest.fn(),
    currentParameters: [
        {
            contactPhone: "1234567890",
            dateOne: Date,
            dateTwo: Date,
            time: 15
        }
    ],
    cdmValue: jest.fn(),
    currentCdmValue: false,
    refreshValue: jest.fn(),
    currentRefreshValue: false
}

describe('LogResultsPage/>', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });
    it("renders correctly", () => {
        const wrapper = shallow(<LogResultsPage {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('finds Header Form', () => {
        const wrapper = shallow(<LogResultsPage {...validProps} />);
        expect(wrapper.find('HeaderForm'));
    });
    it('finds Print Button', () => {
        const wrapper = shallow(<LogResultsPage {...validProps} />);
        expect(wrapper.find('#print-button'));
    });
    it('finds refresh Button and clicks', () => {
        const setStatus = jest.fn();
        const useStatusSpy = jest.spyOn(React, "useState");
        /* eslint-disable no-unused-vars */
        useStatusSpy.mockImplementation(_init => [true, setStatus]);
        const wrapper = shallow(<LogResultsPage {...validProps} />);
        wrapper.find('#refresh-button').simulate("click");
        expect(setStatus).toHaveBeenCalledWith("");
    });
});