import React from "react";
import Proptypes from "prop-types"
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import RefreshIcon from "@material-ui/icons/Refresh";
import PrintIcon from "@material-ui/icons/Print";
import HeaderForm from "../HeaderForm";
import ResultsTable from "../ResultsTable";
import OffboardSuccessCard from "../OffboardSucessCard";
import EncounterResultsContent from "../../content/EncounterResultsContent.json";
import error from "../../assets/images/error.svg";
import "./styles.scss";

const rootClass = "log-results-page";

const {
  pageTitle,
  noLogsFoundText,
  refreshButtonText,
  printButtonText
} = EncounterResultsContent;

const shouldDisplayTable = (logsArray) => {
  return logsArray.length > 0
    ? {
      table: `${rootClass}__tableArea`,
      error: `${rootClass}__none`,
      searchArea: `${rootClass}__searchArea`,
      button: `${rootClass}__refreshButton`,
    }
    : {
      table: `${rootClass}__none`,
      error: `${rootClass}__error`,
      searchArea: `${rootClass}__none`,
      button: `${rootClass}__none`,
    };
};

const printPage = () => {
  window.print();
}

const LogResultsPage = ({
  searchResponse,
  parameters,
  currentParameters,
  cdmValue,
  currentCdmValue,
  currentRefreshValue,
  refreshValue,
}) => {
  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#2A525A",
      },
    },
  });

  const [status, setStatus] = React.useState("");

  const refreshData = () => {
    setStatus("");
    refreshValue(!currentRefreshValue);
  };

  return (
    <div className={`${rootClass}`}>
      <div className={`${rootClass}__title`}>{pageTitle}</div>
      <div className={`${rootClass}__headerArea`}>
        <HeaderForm
          parameters={parameters}
          searchResponse={searchResponse}
          currentParameters={currentParameters}
        />
      </div>
      <div className={shouldDisplayTable(searchResponse).error}>
        <img src={error} alt="error" className={`${rootClass}__image`} />
        <span className={`${rootClass}__errorText`}>{noLogsFoundText}</span>
      </div>
      <div className={shouldDisplayTable(searchResponse).table}>
        <ResultsTable
          logs={searchResponse}
          status={setStatus}
          cdmValue={cdmValue}
          currentCdmValue={currentCdmValue}
        />
      </div>
      <div className={`${rootClass}__buttonArea`}>
        <div className={shouldDisplayTable(searchResponse).button}>
          <ThemeProvider theme={theme}>
            <Button
              id="refresh-button"
              onClick={refreshData}
              style={{
                display: "flex",
                margin: "auto",
                marginTop: "3vh",
                alignContent: "right",
                lineHeight: "2.90em",
                fontSize: "0.75em",
              }}
              variant="contained"
              color="primary"
              startIcon={<RefreshIcon />}
            >
              {refreshButtonText}
            </Button>
          </ThemeProvider>
        </div>
        <div className={shouldDisplayTable(searchResponse).button}>
          <ThemeProvider theme={theme}>
            <Button
              id="print-button"
              onClick={printPage}
              style={{
                display: "flex",
                margin: "auto",
                marginTop: "3vh",
                alignContent: "right",
                lineHeight: "2.90em",
                fontSize: "0.75em",
              }}
              variant="contained"
              color="primary"
              startIcon={<PrintIcon />}
            >
              {printButtonText}
            </Button>
          </ThemeProvider>
        </div>
      </div>
      {status === true && <OffboardSuccessCard message="registered" success/>}
      {status === false && <OffboardSuccessCard message="unregistered" success={false} />}
    </div>
  );
};

export default LogResultsPage;
LogResultsPage.propTypes = {
  searchResponse: Proptypes.arrayOf(Proptypes.shape({})).isRequired,
  parameters: Proptypes.func.isRequired,
  currentParameters: Proptypes.arrayOf(Proptypes.shape({
    contactPhone: Proptypes.string,
    dateOne: Proptypes.date,
    dateTwo: Proptypes.date,
    time: Proptypes.number
  })).isRequired,
  cdmValue: Proptypes.func.isRequired,
  currentCdmValue: Proptypes.oneOfType([
    Proptypes.string,
    Proptypes.bool
  ]).isRequired,
  refreshValue: Proptypes.func.isRequired,
  currentRefreshValue: Proptypes.bool.isRequired
}