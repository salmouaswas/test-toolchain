import PropTypes from "prop-types";
import React from "react";
import useForm from "../../hooks/useForm"
import getUploadToken, { getPhoneNumber } from "../../services/User";
import CommonButton from "../CommonButton";
import "./styles.scss";

const SearchBar = ({ searchBar, secondary, error, success, uploadStatus }) => {
  const rootClass = "searchbar";
  const { buttonText, placeHolderText } = searchBar;


  const stateSchema = {
    number: { value: '', error: '' },
  };

  const validationStateSchema = {
    number: {
      required: true,
    }
  };
  const onSubmitForm = async state => {
    if (window.location.pathname === '/get-upload-token') {
      try {
        const normalizedPhone = state.number.value.replace(/[^\d]/g, "");
        const response = await getUploadToken(
          normalizedPhone
        );
        success(response[0].USER_ID);
        uploadStatus(null);
        error(false)
      } catch (err) {
        error(true);
        success(null);
        uploadStatus(null);
      }
    }
    else if (window.location.pathname === '/offboard-user') {
      try {
        const normalizedPhone = state.number.value.replace(/[^\d]/g, "");
        const response = await getPhoneNumber(
          normalizedPhone
        );
        success(response[0].PHONE_NUMBER);
        error(false)
      } catch (err) {
        error(true);
        success(null);
      }
    }
  };
  const { state, handleOnChange, handleOnSubmit, validate } = useForm(
    stateSchema,
    validationStateSchema,
    onSubmitForm
  );
  const handleSubmit = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      handleOnSubmit(e)
    }
  }
  return (
    <div className={rootClass}>
      <form className={`${rootClass}__form`}>
        <label htmlFor="Phone Number">
          <input
            className={`${rootClass}__form__input`}
            id="Phone Number"
            type="text"
            name="number"
            placeholder={placeHolderText}
            value={state.number.value}
            onChange={handleOnChange}
            onKeyDown={handleSubmit}
            onBlur={validate}
          />
        </label>
        <CommonButton
          text={buttonText}
          secondary={secondary}
          className={`${rootClass}__form__button`}
          onClick={handleOnSubmit}
        />
      </form>
    </div>
  );
};
export default SearchBar;
SearchBar.propTypes = {
  searchBar: PropTypes.shape({
    buttonText: PropTypes.string,
    placeholder: PropTypes.string,
  }).isRequired,
  secondary: PropTypes.bool,
  success: PropTypes.func.isRequired,
  error: PropTypes.func.isRequired
};
SearchBar.defaultProps = {
  secondary: false,
};
