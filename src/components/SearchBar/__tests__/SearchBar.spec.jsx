/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { shallow } from 'enzyme';
import SearchBar from '../index';


const validProps = {
  searchBar: {
    buttonText: 'button',
    placeholder: 'search',
  },
  success: jest.fn(),
  error: jest.fn(),
  secondary: false,
};
describe('<SearchBar/>', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<SearchBar {...validProps} />);
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it('has button', () => {
    const wrapper = shallow(<SearchBar {...validProps} />);
    expect(wrapper.find('ForwardRef'));
  });
  it('simulates on key down', () => {
    const wrapper = shallow(<SearchBar {...validProps} />);
    const input = wrapper.find('input');
    input.simulate('change', { target: { value: '1234567890' } });
    input.simulate('keydown', { preventDefault: jest.fn(), key: "Enter" });
  });
  it('calls the submit based on click', () => {
    const wrapper = shallow(<SearchBar {...validProps} />);
    const input = wrapper.find('input')
    const button = wrapper.find('ForwardRef')
    input.simulate('change', { target: { value: '1234567890' } });
    button.simulate('click', { preventDefault: jest.fn() });
  });
});