import React from 'react';
import { shallow } from 'enzyme';
import OffboardingSuccessCard from '../index';

const validProps = {
  message: 'user offboarded',
};
describe('<OffboardingSuccessCard/>', () => {
  it('renders correctly', () => {
    // eslint-disable-next-line react/jsx-props-no-spreading
    const wrapper = shallow(<OffboardingSuccessCard {...validProps} />);
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it('has image', () => {
    // eslint-disable-next-line react/jsx-props-no-spreading
    const wrapper = shallow(<OffboardingSuccessCard {...validProps} />);
    expect(wrapper.find('greenCheck'));
  });
});
