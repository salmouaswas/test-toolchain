import React from 'react';
import Proptypes from 'prop-types';
import greenCheck from '../../assets/images/greenCheck.svg';
import error from '../../assets/images/alert.svg';
import './styles.scss';

const OffboardSuccessCard = ({ message, success }) => {
  const rootClass = 'success-card';
  return (
    <div className={rootClass}>
      <div className={`${rootClass}__box`}>
        <p className={`${rootClass}__box__message`}>{message}</p>
        {success ? <img src={greenCheck} alt="" aria-hidden /> :  <img src={error} alt="" aria-hidden/> }
      </div>
    </div>
  );
};
export default OffboardSuccessCard;
OffboardSuccessCard.propTypes = {
  message: Proptypes.string.isRequired,
  success: Proptypes.bool,
};

OffboardSuccessCard.defaultProps = {
  success: false
}

