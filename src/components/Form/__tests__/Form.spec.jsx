import React from 'react';
import { shallow, mount } from 'enzyme';
import { Form } from '../index';

const validProps = {
    title: "title",
    subtitle: "subtitle",
    formInputs: {
        phoneNumber: "Phone Number",
        lastEncounter: "Last Enounter",
        firstEncounter: "First Enounter",
        minimumDuration: "Minimum Duration",
        errorMessage: "Last Encounter Must Be After First Encounter"
    },
    durations: [
        {
            value: "Blank",
            label: "--"
        },
        {
            value: 15,
            label: "15 mins"
        },
        {
            value: 30,
            label: "30 mins"
        },
        {
            value: 45,
            label: "45 mins"
        },
        {
            value: 60,
            label: "60 mins"
        },
    ],
    searchButtonText: "GenerateLog",
    parameters: jest.fn()
}

describe('Form/>', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Form {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('finds button', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = mount(<Form {...validProps} />);
        expect(wrapper.find('ForwardRef(Button)').simulate('click'))
    })
    it('shows date error if late enounter before first encounter', () => {

        const today = new Date()
        const newDate = new Date()
        const twoWeeksAgo = new Date(newDate.setDate(today.getDate() - 14))
        const moreThanTwoWeeks = new Date(newDate.setDate(today.getDate() - 15))
        const setState = jest.fn();
        const useStateSpy = jest.spyOn(React, "useState");
        /* eslint-disable no-unused-vars */
        useStateSpy.mockImplementation(_init => [twoWeeksAgo, setState]);
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Form {...validProps} />);
        const lastDatePicker = wrapper.find("PickerWithState").last()
        lastDatePicker.simulate('change', moreThanTwoWeeks);
        expect(setState).toHaveBeenCalledWith(true)

    });
    it('simualtes number change', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Form {...validProps} />);
        const input = wrapper.find('#margin-none');
        input.simulate('change', { target: { value: '1234567890' } });
    });
    it('simualtes time change', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Form {...validProps} />);
        const input = wrapper.find('#standard-select-duration');
        input.simulate('change', { target: { value: 30 } });
    });
    it('simualtes first encounter change', () => {
        const today = new Date()
        const newDate = new Date()
        const twoWeeksAgo = new Date(newDate.setDate(today.getDate() - 14))
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Form {...validProps} />);
        const input = wrapper.find("PickerWithState").first()
        input.simulate('change', twoWeeksAgo);
    });
    it('simualtes last encounter change', () => {
        const today = new Date()
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Form {...validProps} />);
        const input = wrapper.find("PickerWithState").last()
        input.simulate('change', today);
    });
});