import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import {
  createMuiTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import React from "react";
import Proptypes from 'prop-types'

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    margin: "100",
    justifyContent: "space-evenly",
    minWidth: "80vw",
  },
  row: {
    display: "flex",
    flexDirection: "row",
  },
  button: {
    backgroundColor: "2A525A",
  },
}));

export function Form({
  title,
  subtitle,
  formInputs,
  durations,
  searchButtonText,
  parameters,
}) {
  const classes = useStyles();

  const [number, setNumber] = React.useState("");
  const [duration, setDuration] = React.useState(durations[1].value);

  const today = new Date()
  const newDate = new Date()
  const twoWeeksAgo = new Date(newDate.setDate(today.getDate() - 14))

  const [firstDate, setSelectedDate] = React.useState(twoWeeksAgo);
  const [lastDate, setSelectedLastDate] = React.useState(today)
  const [invalidDate, setInvalidDate] = React.useState(false)

  const handleNumberChange = (event) => {
    setNumber(event.target.value);
  };

  const handleChange = (event) => {
    setDuration(event.target.value);
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const handleLastDateChange = (date) => {
    if (date < firstDate) {
      setInvalidDate(true)
    }
    else if (date > firstDate) {
      setInvalidDate(false)
      setSelectedLastDate(date);
    }
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#2A525A",
      },
    },
  });
  const sendParameters = () => {
    const parametersArray = {
      contactPhone: number,
      dateOne: firstDate,
      dateTwo: lastDate,
      time: duration,
    };
    parameters([parametersArray]);
  };
  return (
    <div
      style={{ display: "flex", justifyContent: "center", maxHeight: "70vh" }}
    >
      <Paper>
        <Typography
          style={{ margin: 8, paddingLeft: 20, paddingTop: 20 }}
          variant="h6"
          gutterBottom
        >
          {title}
        </Typography>
        <Typography
          style={{ margin: 8, paddingLeft: 20 }}
          variant="subtitle1"
          gutterBottom
        >
          {subtitle}
        </Typography>
        <form className={classes.root} noValidate autoComplete="off">
          <TextField
            id="margin-none"
            label={formInputs.phoneNumber}
            style={{ margin: 30, width: "30%" }}
            placeholder="123-456-6789"
            value={number}
            onChange={handleNumberChange}
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <div className={classes.row}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid>
                <DatePicker
                  style={{ margin: 30, width: "30%" }}
                  variant="inline"
                  format="yyyy/MM/dd"
                  margin="normal"
                  id="date-picker-inline"
                  label={formInputs.firstEncounter}
                  value={firstDate}
                  onChange={handleDateChange}
                />
                <DatePicker
                  error={invalidDate}
                  margin="normal"
                  style={{ margin: 30, width: "30%" }}
                  variant="inline"
                  id="date-picker-inline"
                  label={formInputs.lastEncounter}
                  format="yyyy/MM/dd"
                  value={lastDate}
                  onChange={handleLastDateChange}
                  helperText={invalidDate && formInputs.errorMessage}
                />
              </Grid>
            </MuiPickersUtilsProvider>
          </div>
          <TextField
            margin="normal"
            style={{ margin: 30, width: "30%" }}
            id="standard-select-duration"
            select
            fullWidth
            label={formInputs.minimumDuration}
            value={duration}
            onChange={handleChange}
          >
            {durations.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <ThemeProvider theme={theme}>
              <Button
                onClick={sendParameters}
                style={{
                  display: "flex",
                  width: "20%",
                  margin: 50,
                  alignContent: "right",
                  lineHeight: "2.90em",
                  fontSize: "0.75em",
                }}
                variant="contained"
                color="primary"
              >
                {searchButtonText}
              </Button>
            </ThemeProvider>
          </div>
        </form>
      </Paper>
    </div>
  );
}
export default Form
Form.propTypes = {
  title: Proptypes.string.isRequired,
  subtitle: Proptypes.string.isRequired,
  formInputs: Proptypes.shape({
    phoneNumber: Proptypes.string,
    lastEncounter: Proptypes.string,
    firstEncounter: Proptypes.string,
    minimumDuration: Proptypes.string,
    errorMessage: Proptypes.string
  }).isRequired,
  durations: Proptypes.arrayOf(Proptypes.shape({
    value: Proptypes.oneOfType([
      Proptypes.string,
      Proptypes.number
    ]),
    label: Proptypes.string
  })).isRequired,
  searchButtonText: Proptypes.string.isRequired,
  parameters: Proptypes.func.isRequired
}