/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { shallow } from "enzyme";
import CommonButton from "../index";

const vaildProps = {
    href: "",
    text: "",
    secondary: false,
    onClick: () => jest.fn(),
    onKeyDown: () => jest.fn(),
    className: ""
};

describe("<CommonButton/>", () => {
    it("renders correctly", () => {
        const wrapper = shallow(<CommonButton {...vaildProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it("secondary button", () => {
        const wrapper = shallow(<CommonButton {...vaildProps} />);
        if (wrapper.hasClass("button-link--secondary")) {
            expect(wrapper.state("secondary").toBe(true));
        }
    });
    it(" has href logic", () => {
        const wrapper = shallow(<CommonButton {...vaildProps} />);
        wrapper.setProps({ href: "#ref" })
        expect(wrapper.find("href"));
    });
});
