import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const CommonButton = React.forwardRef(
  ({ href, text, secondary, className, onClick, onKeyDown }, ref) => {
    let customClass = 'button-link--primary';

    if (secondary) {
      customClass = 'button-link--secondary';
    }

    if (href) {
      return (
        <a
          href={href}
          className={`${className} button-link ${customClass}`}
          ref={ref}
        >
          {text}
        </a>
      );
    }

    return (
      <button
        type='button'
        className={`${className} button-link ${customClass}`}
        ref={ref}
        onClick={onClick}
        onKeyDown={onKeyDown}
      >
        {text}
      </button>
    );
  }
);

CommonButton.propTypes = {
  href: PropTypes.string,
  text: PropTypes.string.isRequired,
  secondary: PropTypes.bool,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func,
  className: PropTypes.string,
};

CommonButton.defaultProps = {
  secondary: false,
  href: null,
  onClick: null,
  onKeyDown: null,
  className: null,
};

export default CommonButton;
