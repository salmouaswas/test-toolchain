import DateFnsUtils from "@date-io/date-fns";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import React from "react";
import Proptypes from "prop-types";
import "./styles.scss";
import search from "../../assets/images/header_search.svg";
import EncounterResultsContent from "../../content/EncounterResultsContent.json";

const { filters, durations } = EncounterResultsContent;

const CustomTextField = withStyles({
  root: {
    "& .MuiInput-underline:before": {
      borderBottomColor: "white",
      borderWidth: 1,
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "white",
    },
  },
})(TextField);

const CustomDatePicker = withStyles({
  root: {
    "& .MuiInput-underline:before": {
      borderBottomColor: "white",
      borderWidth: 1,
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "white",
    },
  },
})(DatePicker);

const HeaderForm = ({ parameters, currentParameters }) => {
  const [number, setNumber] = React.useState(currentParameters[0].contactPhone);
  const [duration, setDuration] = React.useState(currentParameters[0].time);
  const [firstDate, setSelectedDate] = React.useState(
    currentParameters[0].dateOne
  );
  const [lastDate, setSelectedLastDate] = React.useState(
    currentParameters[0].dateTwo
  );
  const [invalidDate, setInvalidDate] = React.useState(false)

  const handleNumberChange = (event) => {
    setNumber(event.target.value);
  };

  const handleChange = (event) => {
    setDuration(event.target.value);
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const handleLastDateChange = (date) => {
    if (date < firstDate) {
      setInvalidDate(true)
    }
    else if (date > firstDate) {
      setInvalidDate(false)
      setSelectedLastDate(date);
    }
  };

  const sendParameters = (e) => {
    e.preventDefault();
    const parametersArray = {
      contactPhone: number,
      dateOne: firstDate,
      dateTwo: lastDate,
      time: duration,
    };
    parameters([parametersArray]);
  };

  const rootClass = "header-form";

  return (
    <>
      <form className={rootClass} noValidate autoComplete="off">
        <div className={`${rootClass}__row`}>
          <CustomTextField
            id="input-text-phone"
            label={filters.phoneNumber}
            color="primary"
            className={`${rootClass}__textfield`}
            placeholder={currentParameters[0].contactPhone}
            value={number}
            onChange={handleNumberChange}
            margin="normal"
            InputLabelProps={{
              className: `${rootClass}__floatingLabel`,
              shrink: true,
            }}
          />
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <CustomDatePicker
              className={`${rootClass}__textfield`}
              variant="inline"
              format="MMM, dd"
              margin="normal"
              id="input-text-first"
              label={filters.firstEncounter}
              value={firstDate}
              onChange={handleDateChange}
              InputLabelProps={{
                className: `${rootClass}__floatingLabel`,
              }}
            />
            <CustomDatePicker
              error={invalidDate}
              margin="normal"
              variant="inline"
              format="MMM, dd"
              className={`${rootClass}__textfield`}
              id="input-text-last"
              label={filters.lastEncounter}
              value={lastDate}
              onChange={handleLastDateChange}
              InputLabelProps={{
                className: `${rootClass}__floatingLabel`,
              }}
              helperText={invalidDate && filters.errorMessage}
            />
          </MuiPickersUtilsProvider>
          <CustomTextField
            margin="normal"
            className={`${rootClass}__textfield`}
            id="input-text-time"
            select
            fullWidth
            label={filters.minimumDuration}
            value={duration}
            onChange={handleChange}
            InputLabelProps={{
              className: `${rootClass}__floatingLabel`,
            }}
          >
            {durations.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </CustomTextField>
          <button
            className={`${rootClass}__button`}
            type="submit"
            onClick={(e) => sendParameters(e)}
          >
            <img src={search} alt="search" />
          </button>
        </div>
      </form>
    </>
  );
};

export default HeaderForm;
HeaderForm.propTypes = {
  currentParameters: Proptypes.arrayOf(Proptypes.shape({
    contactPhone: Proptypes.string,
    dateOne: Proptypes.date,
    dateTwo: Proptypes.date,
    time: Proptypes.number
  }
  )).isRequired,
  parameters: Proptypes.func.isRequired
}