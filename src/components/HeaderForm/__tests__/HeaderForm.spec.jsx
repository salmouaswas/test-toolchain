import React from 'react';
import { shallow } from 'enzyme';
import HeaderForm from '../index';

const today = new Date()
const newDate = new Date()
const twoWeeksAgo = new Date(newDate.setDate(today.getDate() - 14))

const moreThanTwoWeeks = new Date(newDate.setDate(today.getDate() - 15))


const validProps = {
    currentParameters: [
        {
            contactPhone: "1234567890",
            dateOne: twoWeeksAgo,
            dateTwo: today,
            time: 15
        }
    ],
    parameters: jest.fn()
}

describe('Header Form/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('finds button', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        expect(wrapper.find('button').simulate('click', { preventDefault: jest.fn() }))
    });
    it('simualtes number change', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        const input = wrapper.find('#input-text-phone');
        input.simulate('change', { target: { value: '1234567890' } });
    });
    it('simualtes time change', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        const input = wrapper.find('#input-text-time');
        input.simulate('change', { target: { value: 45 } });
    });
    it('simualtes first date change', () => {

        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        const input = wrapper.find('#input-text-first');
        input.simulate('change', twoWeeksAgo);
    });
    it('simualtes last date change', () => {

        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        const input = wrapper.find('#input-text-last');
        input.simulate('change', today);
    });
    it('shows date error if late enounter before first encounter', () => {
        const setState = jest.fn();
        const useStateSpy = jest.spyOn(React, "useState");
        /* eslint-disable no-unused-vars */
        useStateSpy.mockImplementation(_init => [twoWeeksAgo, setState]);
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<HeaderForm {...validProps} />);
        const lastDatePicker = wrapper.find('#input-text-last');
        lastDatePicker.simulate('change', moreThanTwoWeeks);
        expect(setState).toHaveBeenCalledWith(true)

    });
});