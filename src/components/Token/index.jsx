import React from "react";

const Token = ({
  style = {},
  fill = "#000",
  width = "100%",
  className = "",
  viewBox = "0 0 32 32",
}) => (
  <svg
    width={width}
    style={style}
    height={width}
    viewBox={viewBox}
    xmlns="http://www.w3.org/2000/svg"
    className={`svg-icon ${className || ""}`}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      fill={fill}
      d="M30 0L0 13.3333V33.3333C0 51.8333 12.8 69.1333 30 73.3333C47.2 69.1333 60 51.8333 60 33.3333V13.3333L30 0ZM30 36.6333H53.3333C51.5667 50.3667 42.4 62.6 30 66.4333V36.6667H6.66667V17.6667L30 7.3V36.6333Z"
    />
  </svg>
);

export default Token;
