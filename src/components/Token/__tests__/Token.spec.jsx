import React from 'react';
import { shallow } from 'enzyme';
import Token from '../index';

describe('Token/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<Token />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
});