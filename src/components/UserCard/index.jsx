import React from 'react';
import Proptypes from 'prop-types';
import './styles.scss';

const UserCard = ({ phoneNumber }) => {
  const rootClass = 'user-card';
  return (
    <div className={rootClass}>
      <p className={`${rootClass}__number`}>{phoneNumber}</p>
    </div>
  );
};
export default UserCard;
UserCard.propTypes = {
  phoneNumber: Proptypes.string.isRequired,
};
