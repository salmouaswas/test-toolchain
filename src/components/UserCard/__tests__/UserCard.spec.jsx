import React from 'react';
import { shallow } from 'enzyme';
import UserCard from '../index';

const validProps = {
  phoneNumber: '###-###-####',
};
describe('<UserCard/>', () => {
  it('renders correctly', () => {
    // eslint-disable-next-line react/jsx-props-no-spreading
    const wrapper = shallow(<UserCard {...validProps} />);
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
