import React from 'react';
import { shallow } from 'enzyme';
import ErrorMessage from '../index';

const validProps = {
    title: "title",
    buttonText: "button"
}

describe('ErrorMessage/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<ErrorMessage {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('finds button', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<ErrorMessage {...validProps} />);
        expect(wrapper.find('ForwardRef').simulate('click'))
    });

});