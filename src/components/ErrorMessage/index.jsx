import React from 'react'
import Proptypes from 'prop-types'
import { navigate } from '@reach/router';
import CommonButton from '../CommonButton'
import error from '../../assets/images/error.svg';
import './styles.scss'


const ErrorMessage = ({ title, buttonText }) => {
    const rootClass = 'error-message'
    const returnHome = () => {
        navigate('/')
    }
    return (
        <div className={rootClass}>
            <img src={error} alt='error icon' />
            <p className={`${rootClass}__text`}>{title}</p>
            <CommonButton text={buttonText} onClick={returnHome} />
        </div>

    )
};
export default ErrorMessage
ErrorMessage.propTypes = {
    title: Proptypes.string.isRequired,
    buttonText: Proptypes.string.isRequired
}