import React from 'react';
import { shallow } from 'enzyme';
import ResultsTable from '../index';

const validProps = {
    logs: [{
        CENTRAL_USER_ID: 1313134,
        PERIPHERAL_USER_ID: 1313212,
        CLOSE_CONTACT: "5195641231",
        CONTACT_DATE: "2020-04-20",
        TOTAL_ENCOUNTERS: 4,
        LAST_ENCOUNTER: "2020-04-20 12:28:38.000000",
        FIRST_ENCOUNTER: "2020-04-20 10:28:13.000000",
        CONTACT_TIME_SEC: 7225,
        TRANSFERRED_TO_CDM: false
    }],
    status: jest.fn(),
    cdmValue: jest.fn(),
    currentCdmValue: false,

}

describe('Results Table/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<ResultsTable  {...validProps} />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it('handles user change in search', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<ResultsTable  {...validProps} />);
        const input = wrapper.find('input')
        input.simulate("change", { target: { value: "1234" } })
    });
    it('clears search', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<ResultsTable  {...validProps} />);
        const button = wrapper.find('button').first()
        button.simulate("click")
    });
});