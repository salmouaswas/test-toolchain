/* eslint-disable no-console */
/* eslint-disable radix */
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from '@material-ui/core/TableSortLabel';
import React from "react";
import Proptypes from "prop-types"
import EncounterResultsContent from "../../content/EncounterResultsContent.json";
import UncheckedIcon from "../../assets/images/checkbox-unchecked.svg";
import CheckedIcon from "../../assets/images/checkbox-checked.svg";
import cross from "../../assets/images/cross.svg";
import "./style.scss";
import { changeTransferredToCDM } from "../../services/User";

const useStyles = makeStyles({
  table: {
    maxWidth: "90vw",
  },
});

const { tableHeaders } = EncounterResultsContent;

export default function ResultsTable({ logs, status, cdmValue, currentCdmValue }) {
  const classes = useStyles();
  const rootClass = "results-table";
  const [searchAreaColor, setSearchAreaColor] = React.useState({
    backgroundColor: "#FFF",
  });
  const [searchValue, setSearchValue] = React.useState("");
  const [filteredLogs, setLogs] = React.useState(logs);
  const [order, setOrder] = React.useState('asc');
  const [orderType, setOrderType] = React.useState('date');
  const [orderBy, setOrderBy] = React.useState('CONTACT_DATE');

  const clearSearch = () => {
    setSearchValue("");
    setSearchAreaColor({ backgroundColor: "#FFF" });
  };
  const handleSearchChange = (e) => {
    setSearchValue(e.target.value);
    if (e !== "") {
      setSearchAreaColor({ backgroundColor: "#B6E4EE" });
    } else if (e === "" || e.target.value === "") {
      setSearchAreaColor({ backgroundColor: "#FFF" });
    }
  };

  const changeStatus = async (currentBoolean, central, closeContact) => {
    try {
      const response = await changeTransferredToCDM(
        !currentBoolean,
        central,
        closeContact
      );
      if (response) {
        cdmValue(!currentCdmValue);
        status(!currentBoolean);
      }
    } catch (err) {
      console.log(err);
    }
  };

  React.useEffect(
    () => {
      const response = logs.filter((log) =>
        log.CLOSE_CONTACT && log.CLOSE_CONTACT.startsWith(searchValue)
      );
      setLogs(response);
    },
    [searchValue, logs]
  );

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property.id && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property.id);
    setOrderType(property.type);
  };

  function stableSort(logArray) {
    let orderedArray = [];
    switch (orderType) {
      case "date":
        if (order === 'asc') {
          orderedArray = logArray.sort((a, b) => {
            return new Date(a[orderBy]) - new Date(b[orderBy])
          })
        } else {
          orderedArray = logArray.sort((a, b) => { return new Date(b[orderBy]) - new Date(a[orderBy]) })
        }
        break;
      case "number":
      case "boolean":
        if (order === 'asc') {
          orderedArray = logArray.sort((a, b) => { return Number(a[orderBy]) - Number(b[orderBy]) })
        } else {
          orderedArray = logArray.sort((a, b) => { return Number(b[orderBy]) - Number(a[orderBy]) })
        }
        break;
      default:
        orderedArray = logArray;
    }
    return orderedArray;
  }


  const formatTime = (time) => {
    const format = time.split(" ");
    const removeYear = format[1];
    const string = ":";
    const formatRemoveYear = removeYear.split(":");
    const newString = formatRemoveYear[0] + string + formatRemoveYear[1];
    return newString;
  };
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {tableHeaders.filter(header => !header.sortable).map((header) => (
              <TableCell
                key={header.id}
                sortDirection={orderBy === header.id ? order : false}
                align="center"
                style={{ borderBottom: "none" }}
                fontWeight='500'
              >
                {header.label}
              </TableCell>
            ))}
            {tableHeaders.filter(header => header.sortable).map((header) => (
              <TableCell
                key={header.id}
                sortDirection={orderBy === header.id ? order : false}
                align="center"
                style={{ borderBottom: "none" }}
                fontWeight='500'
              >
                <TableSortLabel
                  active={orderBy === header.id}
                  direction={orderBy === header.id ? order : 'asc'}
                  onClick={() => handleRequestSort(header)}
                  hideSortIcon={false}
                >
                  {header.label}
                </TableSortLabel>
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          <tr className={`${rootClass}__searchArea`}>
            <th
              htmlFor="Close contact number"
              className={`${rootClass}__searchArea__head`}
            >
              <input
                style={searchAreaColor}
                className={`${rootClass}__searchArea__head__search`}
                id="CloseContact"
                type="text"
                placeholder="Search"
                onChange={handleSearchChange}
                value={searchValue}
              />
            </th>
            <td>
              <button
                type="submit"
                className={`${rootClass}__searchArea__cross`}
                onClick={clearSearch}
              >
                <img src={cross} alt="undo search" />
              </button>
            </td>
          </tr>
          {stableSort(filteredLogs)
            .map((row, index) => (
              <TableRow key={`${row.PERIPHERAL_USER_ID} ${index + 1}`}>
                <TableCell
                  className={`${rootClass}__values`}
                  align="center"
                  component="th"
                  scope="row"
                >
                  {row.CLOSE_CONTACT}
                </TableCell>
                <TableCell className={`${rootClass}__values`} align="center">
                  {row.CONTACT_DATE}
                </TableCell>
                <TableCell className={`${rootClass}__values`} align="center">
                  {formatTime(row.FIRST_ENCOUNTER)}
                </TableCell>
                <TableCell className={`${rootClass}__values`} align="center">
                  {formatTime(row.LAST_ENCOUNTER)}
                </TableCell>
                <TableCell className={`${rootClass}__values`} align="center">
                  {parseInt(row.CONTACT_TIME_SEC / 60)}
                </TableCell>
                <TableCell align="center">
                  <button
                    className={`${rootClass}__checkmark`}
                    type="button"
                    onClick={() =>
                      changeStatus(
                        row.TRANSFERRED_TO_CDM,
                        row.CENTRAL_USER_ID,
                        row.PERIPHERAL_USER_ID
                      )
                    }
                  >
                    <img
                      src={
                        row.TRANSFERRED_TO_CDM
                          ? CheckedIcon
                          : UncheckedIcon
                      }
                      alt="check"
                    />
                  </button>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
ResultsTable.propTypes = {
  logs: Proptypes.arrayOf(Proptypes.shape({})).isRequired,
  status: Proptypes.func.isRequired,
  cdmValue: Proptypes.func.isRequired,
  currentCdmValue: Proptypes.oneOfType([
    Proptypes.string,
    Proptypes.bool
  ]).isRequired,
}
