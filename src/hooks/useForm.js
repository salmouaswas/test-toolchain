import { useState, useCallback } from 'react';

function useForm(
    stateSchema,
    validationSchema = {},
    callback,

) {
    const [state, setState] = useState(stateSchema);

    const validateState = useCallback(() => {
        const hasErrorInState = Object.keys(validationSchema).some(key => {
            const isInputFieldRequired = validationSchema[key].required;
            const stateValue = state[key].value; // state value
            const stateError = state[key].error; // state error

            return (isInputFieldRequired && !stateValue) || stateError;
        });

        return hasErrorInState;
    }, [state, validationSchema]);

    const handleOnChange = useCallback(
        event => {
            const { name } = event.target;
            const { value } = event.target;

            setState(prevState => ({
                ...prevState,
                [name]: { value, error: '' }
            }));
        },
        []
    );

    const validate = useCallback(
        event => {
            const { name } = event.target;
            const { value } = event.target;
            let error = '';

            if (
                value &&
                validationSchema[name].validator !== null &&
                typeof validationSchema[name].validator === 'object'
            ) {
                if (value && !validationSchema[name].validator.regEx.test(value)) {
                    error = validationSchema[name].validator.error;
                }
            }

            setState(prevState => ({
                ...prevState,
                [name]: { value, error }
            }));
        },
        [validationSchema]
    );

    const handleOnSubmit = useCallback(
        event => {
            event.preventDefault();

            if (!validateState()) {
                callback(state, setState);
            } else {
                callback(false, setState);
            }
        },
        [state, callback, validateState]
    );

    return {
        state,
        handleOnChange,
        handleOnSubmit,
        validate
    };
}

export default useForm;