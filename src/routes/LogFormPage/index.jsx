import React from "react";
import { searchEcounter } from "../../services/User/index";
import { Form } from "../../components/Form";
import EncounterLogContentForm from "../../content/EncounterLogContentForm.json";
import LogResultsPage from "../../components/LogResultsPage";
import "./style.scss";


const rootClass = "log-form-page";

const {
  title,
  subtitle,
  formInputs,
  durations,
  searchButtonText,
} = EncounterLogContentForm;

const LogFormPage = () => {
  const [hasSearched, setHasSearched] = React.useState(false);
  const [searchResponse, setSearchResponse] = React.useState([]);
  const [parameters, setParameters] = React.useState([]);
  const [cdmValue, setCdmValue] = React.useState("");
  const [refreshValue, setRefreshValue] = React.useState(false);

  React.useEffect(
    () => {
      const getResults = async () => {
        const normalizedPhone = parameters[0].contactPhone.replace(/[^\d]/g, "");
        try {
          const response = await searchEcounter(
            normalizedPhone,
            parameters[0].dateOne,
            parameters[0].dateTwo,
            parameters[0].time
          );
          if (response && response.length !== undefined) {
            setSearchResponse(response);
          }
          setHasSearched(true);
        } catch (err) {
          // eslint-disable-next-line no-console
          console.log(err);
        }
      };

      if (parameters.length > 0) {
        getResults();
      }
    },
    [parameters, cdmValue, refreshValue]
  );

  return (
    <>
      {!hasSearched ? (
        <div className={`${rootClass}`}>
          <Form
            title={title}
            subtitle={subtitle}
            formInputs={formInputs}
            durations={durations}
            searchButtonText={searchButtonText}
            hasSearched={setHasSearched}
            parameters={setParameters}
          />
        </div>
      ) : (
          <LogResultsPage
            searchResponse={searchResponse}
            parameters={setParameters}
            currentParameters={parameters}
            cdmValue={setCdmValue}
            currentCdmValue={cdmValue}
            currentRefreshValue={refreshValue}
            refreshValue={setRefreshValue}
          />
        )}
    </>
  );
};

export default LogFormPage;
