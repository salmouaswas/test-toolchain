import React from 'react';
import { shallow } from 'enzyme';
import LogFormPage from '../index';

describe("<LogFormPage/>", () => {
    let wrapper;
    beforeEach(() => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        wrapper = shallow(< LogFormPage />);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it("renders correctly", () => {
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it("Simulates submitting form to change display ", () => {
        const setState = jest.fn();
        const useStateSpy = jest.spyOn(React, "useState");
        /* eslint-disable no-unused-vars */
        wrapper = shallow(< LogFormPage />);
        useStateSpy.mockImplementation(_init => [true, setState]);
        expect(wrapper.debug()).toMatchSnapshot();
    });
});