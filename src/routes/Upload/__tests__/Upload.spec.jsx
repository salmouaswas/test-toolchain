
import React from "react";
import { shallow } from "enzyme";
import Upload from "../index";


describe("<Upload/>", () => {
    let wrapper;
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, "useState");
    /* eslint-disable no-unused-vars */
    useStateSpy.mockImplementation(_init => [false, setState]);

    beforeEach(() => {
        wrapper = shallow(<Upload />);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it("renders correctly", () => {
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it("finds searchbar", () => {
        expect(wrapper.find("SearchBar"));
    });
    it("shows code token on state change", () => {
        if (wrapper.hasClass("upload__container__instruction")) {
            expect(setState).toHaveBeenCalled();
        }
    });
});