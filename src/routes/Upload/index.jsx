import React from "react";
import alert from "../../assets/images/alert.svg";
import SearchBar from "../../components/SearchBar";
import { sendUpload } from "../../services/User";
import GetUploadTokenContent from "../../content/GetUploadTokenContent.json";
import "./styles.scss";
import CommonButton from "../../components/CommonButton";
import OffboardSuccessCard from "../../components/OffboardSucessCard";

const Upload = () => {
  const rootClass = "upload";
  const { title, instruction, failureMessage, searchBar, uploadSuccessful, uploadFailure, sendSMSText, tokenNotFound } = GetUploadTokenContent;
  const [success, setSuccess] = React.useState(null);
  const [notfound, setNotFound] = React.useState(false);
  const [uploadStatus, setUploadStatus] = React.useState(null);
  const [uploadSuccess, setUploadSuccess] = React.useState(false);

  const triggerUpload = async () => {
    const response = await sendUpload(success);
    if(response.error) {
      if(response.status === 404) {
        setUploadStatus(tokenNotFound)
      } else {
        setUploadStatus(uploadFailure);
      }
      setUploadSuccess(false);
    } else {
      setUploadStatus(uploadSuccessful);
      setUploadSuccess(true);
    }
  }

  return (
    <div className={rootClass}>
      <h3 className={`${rootClass}__header`}>{title}</h3>
      <div>
        <SearchBar searchBar={searchBar} success={setSuccess} uploadStatus={setUploadStatus} error={setNotFound} />
      </div>
      {success && (
        <div className={`${rootClass}__container`}>
          <p className={`${rootClass}__container__instruction`}>
            {instruction}
          </p>
          <p className={`${rootClass}__container__code`}>{success}</p>
          {uploadStatus === null ?
            <CommonButton
            text={sendSMSText}
            onClick={() => triggerUpload()}
            /> :
            <OffboardSuccessCard message={uploadStatus} success={uploadSuccess}/>
          }
         
        </div>
      )}
      {notfound && (
        <div className={`${rootClass}__error-message`}>
          <img src={alert} alt="error" />
          <p className={`${rootClass}__error-message__text`}>
            {failureMessage}
          </p>
        </div>
      )}
    </div>
  );
};
export default Upload;
