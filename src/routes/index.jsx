import React from 'react';
import { Router } from '@reach/router';
import Upload from './Upload';
import Offboarding from './Offboarding';
import Header from '../components/Header'
import LandingPage from './LandingPage';
import LogFormPage from './LogFormPage';
import ErrorPage from './ErrorPage';


const Routes = () => {
  return (
    <>
      <Header />
      <Router>
        <LandingPage path='/' />
        <LogFormPage path='/encounter-log' />
        <Upload path='/get-upload-token' />
        <Offboarding path='/offboard-user' />
        <ErrorPage default />
      </Router>
    </>
  );
};

export default Routes;

