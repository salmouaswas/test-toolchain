import React from "react";
import HomeCards from "../../components/HomeCards";
import HomeContent from "../../content/HomeContent.json";
import "./style.scss";

const { cards, pageTitle } = HomeContent;
const rootClass = "landing-page";

const LandingPage = () => {
  return (
    <div className={`${rootClass}`}>
      <div className={`${rootClass}__title`}>
          {pageTitle}
      </div>
      <div className={`${rootClass}__cardArea`}>
        <HomeCards cards={cards} />
      </div>
    </div>
  );
};
export default LandingPage;
