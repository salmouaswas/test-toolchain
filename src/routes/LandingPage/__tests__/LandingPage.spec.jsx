import React from 'react';
import { shallow } from 'enzyme';
import LandingPage from '../index';

describe('LandingPage/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<LandingPage />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
});