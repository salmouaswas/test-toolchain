import React from 'react';
import { shallow } from 'enzyme';
import ErrorPage from '../index';

describe('ErrorPage/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = shallow(<ErrorPage />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
});