import React from 'react';
import ErrorPageContent from '../../content/ErrorPageContent.json';
import ErrorMessage from '../../components/ErrorMessage'
import './styles.scss';

const ErrorPage = () => {
    const rootClass = 'error-page';
    const { title, buttonText } = ErrorPageContent
    return (
        <div className={rootClass}>
            <ErrorMessage title={title} buttonText={buttonText} />
        </div>
    )
};
export default ErrorPage