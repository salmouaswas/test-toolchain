import React from "react";
import { getOffboardStatus } from "../../services/User";
import alert from "../../assets/images/alert.svg";
import check from "../../assets/images/check.svg";
import CommonButton from "../../components/CommonButton";
import OffboardSuccessCard from "../../components/OffboardSucessCard";
import SearchBar from "../../components/SearchBar";
import UserCard from "../../components/UserCard";
import OffboardingContent from "../../content/OffboardingContent.json";
import "./styles.scss";

const Offboarding = () => {
  const rootClass = "offboard";
  const {
    title,
    failureMessage,
    successMessage,
    userFound,
    searchBar,
    OffboardButtonText
  } = OffboardingContent;
  const [success, setSuccess] = React.useState(null);
  const [notFound, setnotFound] = React.useState(false);
  const [complete, setComplete] = React.useState(false);
  const showSuccessMessage = () => {
    if (success) {
      try {
        const response = getOffboardStatus(success);
        if (response) {
          setSuccess(false);
          setComplete(true);
        }
      }
      catch (err) {
        setComplete(false)
      }
    }
  };
  return (
    <div className={rootClass}>
      <h3 className={`${rootClass}__header`}>{title}</h3>
      <div>
        <SearchBar
          searchBar={searchBar}
          secondary
          success={setSuccess}
          error={setnotFound}
        />
      </div>
      {success && (
        <div className={`${rootClass}__success-state`}>
          <div className={`${rootClass}__success-state__message`}>
            <img src={check} alt="check" />
            <p className={`${rootClass}__success-state__message__text`}>
              {userFound}
            </p>
          </div>
          <div className={`${rootClass}__success-state__offboard`}>
            <UserCard phoneNumber={success} />
            <CommonButton
              text={OffboardButtonText}
              onClick={showSuccessMessage}
            />
          </div>
        </div>
      )}
      {complete && !success && !notFound ? <OffboardSuccessCard message={successMessage} success /> : null}
      {notFound && (
        <div className={`${rootClass}__error-message`}>
          <img src={alert} alt="error" />
          <p className={`${rootClass}__error-message__text`}>
            {failureMessage}
          </p>
        </div>
      )}
    </div>
  );
};
export default Offboarding;
