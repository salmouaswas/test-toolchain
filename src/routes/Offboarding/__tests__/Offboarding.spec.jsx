import React from "react";
import { shallow } from "enzyme";
import Offbaording from "../index";


describe("<Offbaording/>", () => {
    let wrapper;
    const setState = jest.fn();
    const useStateSpy = jest.spyOn(React, "useState");
    /* eslint-disable no-unused-vars */
    useStateSpy.mockImplementation(_init => [false, setState]);
    const setNotFoundState = jest.fn();
    const useNotFoundStateSpy = jest.spyOn(React, "useState");
    /* eslint-disable no-unused-vars */
    useNotFoundStateSpy.mockImplementation(_init => [false, setState]);
    const setCompleteState = jest.fn();
    const useCompleteStateSpy = jest.spyOn(React, "useState");
    /* eslint-disable no-unused-vars */
    useCompleteStateSpy.mockImplementation(_init => [false, setState]);

    beforeEach(() => {
        wrapper = shallow(<Offbaording />);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it("renders correctly", () => {
        expect(wrapper.debug()).toMatchSnapshot();
    });
    it("finds searchbar", () => {
        expect(wrapper.find("SearchBar"));
    });
    it("shows user if user found", () => {
        if (wrapper.hasClass("offboard__success-state")) {
            expect(setState).toHaveBeenCalled();
        }
    });
    it("shows success message if user found", () => {
        if (wrapper.hasClass("offboard__success-state")) {
            expect(setState).toHaveBeenCalled();
            wrapper.find("ForwardRef").last().simulate("click")
            expect(setCompleteState).toHaveBeenCalled();
            expect(wrapper.find("OffboardSuccessCard"))
        }
    });
    it("shows not found message if user is not found", () => {
        if (wrapper.hasClass("offboard__error-message")) {
            expect(setNotFoundState).toHaveBeenCalled();
        }
    });
});