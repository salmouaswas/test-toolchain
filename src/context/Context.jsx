import React from 'react';

export const initialState = {
  content: {},
  setContent: () => {}
};

export const Context = React.createContext(initialState);
