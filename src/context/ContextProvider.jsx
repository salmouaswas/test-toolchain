import React from 'react';
import { Context } from './Context';

// eslint-disable-next-line react/prop-types
const ContextProvider = ({ children, value }) => (
  <Context.Provider value={value}>{children}</Context.Provider>
);

export default ContextProvider;
