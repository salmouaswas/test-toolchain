import { get, getHeaders } from "..";

describe("Test utils", () => {
  test("get headers", async (done) => {
    const response = await getHeaders();
    expect(response).toMatchObject({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "http:localhost:3000"
    })
    done();
  });

  test("get when headers match", async (done) => {
    const mockSuccessResponse = { data: 'heres the data' };
    const mockJsonPromise = Promise.resolve(mockSuccessResponse); 
    const mockFetchPromise = Promise.resolve({
      headers: { get: jest.fn(() => ({
        match: jest.fn(() => true)
      }))},
      json: () => mockJsonPromise,
    });
    global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);
    const response = await get('www.test.ca', getHeaders());
    expect(response).toMatchObject({ data: 'heres the data' })
    done();
  });
});
