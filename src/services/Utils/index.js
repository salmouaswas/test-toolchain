export const get = (url, headers) =>
    fetch(url, {
        method: "GET",
        headers
    }).then(res => {
        if (
            res.headers.get("content-type") &&
            res.headers.get("content-type").match(/application\/json/)
        ) {
            return res.json();
        }
        return res;
    });

export const getHeaders = () => ({
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "http:localhost:3000"
});
