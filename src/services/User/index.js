import { get, getHeaders } from "../Utils";

const getUploadToken = async phoneNumber => {
    const response = await get(`/getUser/${phoneNumber}`, getHeaders())
    if (response.err) {
        throw new Error('User Not Found');
    } else {
        return response;
    }
};

export const sendUpload = async userId => {
    const response = await get(`/triggerUpload/${userId}`, getHeaders())
    if (!response.ok) {
        return { error: true, status: response.status }
    }
    return response;
};
export const getPhoneNumber = async phoneNumber => {
    const response = await get(`/getPhoneNumber/${phoneNumber}`, getHeaders())
    if (response.err) {
        throw new Error('User Not Found');
    } else {
        return response;
    }
};
export const getOffboardStatus = async phoneNumber => {
    const response = await get(`/offboard/${phoneNumber}`, getHeaders())
    if (response.err) {
        throw new Error('User Not offboarded');
    } else {
        return response;
    }
}
export const changeTransferredToCDM = async (boolean, centralId, peripheralId) => {
    const response = await get(`/updateCDMStatus/${boolean}/${centralId}/${peripheralId}`, getHeaders())
    if (response.err) {
        throw new Error('Update Unsuccessful');
    } else {
        return response;
    }
}
export const searchEcounter = async (phoneNumber, firstEncounter, lastEncounter, duration) => {
    const response = await get(`/search/${phoneNumber}/${firstEncounter}/${lastEncounter}/${duration}`, getHeaders())
    if (response.err) {
        throw new Error('Search Unsuccessful');
    } else {
        return response;
    }
}

export default getUploadToken