import getUploadToken, { getPhoneNumber, getOffboardStatus, changeTransferredToCDM, searchEcounter} from '..';
import { get } from '../../Utils';

jest.mock('../../Utils', () => {
  return {
    get: jest.fn().mockResolvedValue({}),
    getHeaders: jest.fn(() => ({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "http:localhost:3000"
    }))
  }
});

describe("Test calls to the server", () => {
  test("should respond successfully to getting upload token", async (done) => {
    const response = await getUploadToken('122222222');
    expect(response).toMatchObject({})
    done();
  });
  test("should respond with a failure for upload token", async (done) => {
    get.mockImplementation().mockResolvedValue({ err: 'error'});
    let error;
    try {
      await getUploadToken('122222222');
    } catch (e) {
      error = "error";
    }
    expect(error).toEqual("error");
    done();
  });
  test("should respond successfully to getting phone number", async (done) => {
    get.mockImplementation().mockResolvedValue({});
    const response = await getPhoneNumber('122222222');
    expect(response).toMatchObject({})
    done();
  });
  test("should respond with a failure for getting phone number", async (done) => {
    get.mockImplementation().mockResolvedValue({ err: 'error'});
    let error;
    try {
      await getPhoneNumber('122222222');
    } catch (e) {
      error = "error";
    }
    expect(error).toEqual("error");
    done();
  });
  test("should respond successfully to getting offboard status", async (done) => {
    get.mockImplementation().mockResolvedValue({});
    const response = await getOffboardStatus('122222222');
    expect(response).toMatchObject({})
    done();
  });
  test("should respond with a failure for getting offboard status", async (done) => {
    get.mockImplementation().mockResolvedValue({ err: 'error'});
    let error;
    try {
      await getOffboardStatus('122222222');
    } catch (e) {
      error = "error";
    }
    expect(error).toEqual("error");
    done();
  });
  test("should respond successfully to changing transferr to CDM flag", async (done) => {
    get.mockImplementation().mockResolvedValue({});
    const response = await changeTransferredToCDM(true, '1234', '3455');
    expect(response).toMatchObject({})
    done();
  });
  test("should respond with a failure to changing transferr to CDM flag", async (done) => {
    get.mockImplementation().mockResolvedValue({ err: 'error'});
    let error;
    try {
      await changeTransferredToCDM(true, '1234', '3455');
    } catch (e) {
      error = "error";
    }
    expect(error).toEqual("error");
    done();
  });
  test("should respond successfully to searching for an encounter", async (done) => {
    get.mockImplementation().mockResolvedValue({});
    const response = await searchEcounter('3322222222', '20-03-04', '20-05-04', '1800');
    expect(response).toMatchObject({})
    done();
  });
  test("should respond with a failure to changing transferr to CDM flag", async (done) => {
    get.mockImplementation().mockResolvedValue({ err: 'error'});
    let error;
    try {
      await searchEcounter('3322222222', '20-03-04', '20-05-04', '1800');
    } catch (e) {
      error = "error";
    }
    expect(error).toEqual("error");
    done();
  });
});