import React, { useState } from "react";
import ContextProvider from "./context/ContextProvider";
import Routes from "./routes/index"
import "./App.scss";

function App() {
  const [content, setContent] = useState({});
  return (
    <ContextProvider value={{
      content,
      setContent
    }}>
     <Routes/>
    </ContextProvider>
  );
}

export default App;
