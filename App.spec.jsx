import React from 'react';
import { mount } from 'enzyme';
import App from './src/App'

describe('<App/>', () => {
    it('renders correctly', () => {
        // eslint-disable-next-line react/jsx-props-no-spreading
        const wrapper = mount(<App />);
        expect(wrapper.debug()).toMatchSnapshot();
    });
});
