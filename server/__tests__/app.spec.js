const request = require("supertest");
const app = require("../server");
const {
  getUserFromNumber,
  offboardUserFromNumber,
  findUser,
  updateRegisteredCMD,
  searchEncounter
} = require("../controllers");

const { CustomError } = require('../utils');

jest.mock('../controllers', () => {
  return {
    getUserFromNumber: jest.fn(() => [{ user_id: '12345'}]),
    offboardUserFromNumber: jest.fn(() => "true"),
    findUser: jest.fn(() => ({'USER_ID': 3, 'PHONE_NUMBER': '4474444444'})),
    updateRegisteredCMD: jest.fn(() => true),
    authIdentity: jest.fn(() => `> Email: test@em.ca ProviderID: 123 <`),
    searchEncounter: jest.fn(() => [{}])
  }
});

describe("Test express routes", () => {
  beforeEach(() => jest.clearAllMocks());
  test("should respond successfully to getting phone number", async (done) => {
    const response = await request(app).get("/getUser/4667777777");
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe("[{\"user_id\":\"12345\"}]");
    done();
  });
  test("should respond with an error if getting phone number does not work", async (done) => {
    getUserFromNumber.mockImplementation(() => {throw new CustomError('Error', 500)} );
    const response = await request(app).get("/getUser/4667777777");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("sanity check test for getting phone number", async (done) => {
    // eslint-disable-next-line no-throw-literal
    getUserFromNumber.mockImplementation(() => {throw 'error';});
    const response = await request(app).get("/getUser/1234");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Something went wrong');
    done();
  });  
  test("should respond successfully to offboarding", async (done) => {
    getUserFromNumber.mockImplementation(() => jest.fn(() => '123456') );
    const response = await request(app).get("/offboard/4667777777");
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe("true");
    done();
  });
  test("should respond with an error if offboarding does not work because of get user from number", async (done) => {
    getUserFromNumber.mockImplementation(() => {throw new CustomError('Error', 500)} );
    const response = await request(app).get("/offboard/4667777777");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("should respond with an error if offboarding does not work because of offboard user from number", async (done) => {
    offboardUserFromNumber.mockImplementation(() => {throw new CustomError('Error', 500)} );
    const response = await request(app).get("/offboard/4667777777");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("sanity check test for offboarding", async (done) => {
    // eslint-disable-next-line no-throw-literal
    getUserFromNumber.mockImplementation(() => {throw 'error';});
    const response = await request(app).get("/offboard/4667777777");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Something went wrong');
    done();
  });  
  test("should respond successfully to get phone number", async (done) => {
    const response = await request(app).get("/getPhoneNumber/4667777777");
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe("{\"USER_ID\":3,\"PHONE_NUMBER\":\"4474444444\"}");
    done();
  });
  test("should respond with an error if getting phone number does not work", async (done) => {
    findUser.mockImplementation(() => {throw new CustomError('Error', 500)} );
    const response = await request(app).get("/getPhoneNumber/4667777777");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("sanity check test for getting phone number", async (done) => {
    // eslint-disable-next-line no-throw-literal
    findUser.mockImplementation(() => {throw 'error';});
    const response = await request(app).get("/getPhoneNumber/4667777777");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Something went wrong');
    done();
  });  
  test("should respond successfully to update CDM status", async (done) => {
    const response = await request(app).get("/updateCDMStatus/true/1234/1234");
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe("{\"change\":true}");
    done();
  });
  test("should respond with an error if updating CDM does not work", async (done) => {
    updateRegisteredCMD.mockImplementation(() => {throw new CustomError('Error', 500)} );
    const response = await request(app).get("/updateCDMStatus/true/1234/1234");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("sanity check test for updating CDM", async (done) => {
    // eslint-disable-next-line no-throw-literal
    updateRegisteredCMD.mockImplementation(() => {throw 'error';});
    const response = await request(app).get("/updateCDMStatus/true/1234/1222");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Something went wrong');
    done();
  });  
  test("should respond successfully to get encounters", async (done) => {
    getUserFromNumber.mockImplementation(() => jest.fn(() => '123456') );
    const response = await request(app).get("/search/4167377777/20-03-02/20-03-04/1800");
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe("[{}]");
    done();
  });
  test("should respond with an error if getting user does not work", async (done) => {
    searchEncounter.mockImplementation(() => {throw new CustomError('Error', 500)} );
    const response = await request(app).get("/search/4167377777/20-03-02/20-03-04/1800");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Error');
    done();
  });
  test("sanity check test for ", async (done) => {
    // eslint-disable-next-line no-throw-literal
    searchEncounter.mockImplementation(() => {throw 'error';});
    const response = await request(app).get("/updateCDMStatus/true/1234/1222");
    expect(response.statusCode).toBe(500);
    expect(response.text).toBe('Something went wrong');
    done();
  });
});

export {};