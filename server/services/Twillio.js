/* eslint-disable no-console */
const { TWILLIO_TOKEN, ACCOUNT_SID, DEFAULT_NUMBER } = process.env;
const client = require('twilio')(ACCOUNT_SID, TWILLIO_TOKEN);

async function sendMessage(phoneNumber, token) {
  const body = `To upload your data to Alberta Health Services, please enter this code into the app: ${token}`
  const messageObject = {
    body,
    messagingServiceSid: DEFAULT_NUMBER,
    to: phoneNumber
  }
  return new Promise((resolve, reject) => {
    client.messages
    .create(messageObject)
    .then((message) => {
      console.log("SMS message sent", message.sid)
      resolve()
    })
    .catch((error) => {
      if (error.code === 20429) {
        console.error("SMS message error, trying again", error)

        // try to send a message again before returning an error
        client.messages
        .create(messageObject)
        .then((message) => {
          console.log("SMS message sent", message.sid)
          resolve()
        })
        .catch((secondError) => {
          console.error("SMS message error, trying again", secondError)
          reject();
        });
      } else {
        reject();
      }
    });
  })
}

module.exports = { sendMessage };
