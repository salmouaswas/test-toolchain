/* eslint-disable no-console */
const ibmdb = require("ibm_db")

const { DATABASE, DB_HOST, DB_USER, DB_PWD, DB_PORT, DB_SCHEMA } = process.env
const connectionConfig = `DATABASE=${DATABASE};HOSTNAME=${DB_HOST ? DB_HOST.split(',')[0] : ''};CURRENTSCHEMA=${DB_SCHEMA};UID=${DB_USER};PWD=${DB_PWD};PORT=${DB_PORT}`
const alternateConnectionConfig = `DATABASE=${DATABASE};HOSTNAME=${DB_HOST ? DB_HOST.split(',')[1] : ''};CURRENTSCHEMA=${DB_SCHEMA};UID=${DB_USER};PWD=${DB_PWD};PORT=${DB_PORT}`

let activeConfig = true;

class DatabaseInstance {
    // connection

    establishConnection() {
        let initialConfig = connectionConfig;
        let alternateConfig = alternateConnectionConfig;
        // if the flag is true, then switch to alternate first
        if (!activeConfig) {
            initialConfig = alternateConnectionConfig;
            alternateConfig = connectionConfig;
        }
        return new Promise((resolve, reject) => {
            ibmdb.open(initialConfig, (err, conn) => {
                if (err) {
                    console.error('Error connecting to database once', err)
                    // validate that this is a communication error sqlcode, if so, try again with main database
                    if (err.sqlcode === -30081 || err.sqlcode === -30080 || err.sqlcode === -4499) {
                        ibmdb.open(initialConfig, (secondError, secondConnection) => {
                            if (err) {
                                console.error('Error connecting to database twice', secondError)
                                // validate that this is a communication error sqlcode, if so, try with recovery database
                                if (secondError.sqlcode === -30081 || secondError.sqlcode === -30080 || secondError.sqlcode === -4499) {
                                    ibmdb.open(alternateConfig, (thirdError, thirdConnection) => {
                                        if (err) {
                                            console.error('Error connecting to second database', thirdError)
                                            reject()
                                        } else {
                                            this.connection = thirdConnection;

                                            // switch flag so that we use this alternateConfig as the initial config going forward while our main db is down
                                            activeConfig = !activeConfig;
                                            resolve();
                                        }
                                    })
                                } else {
                                    reject()
                                }
                            } else {
                                this.connection = secondConnection
                                resolve()
                            }
                        })
                    } else {
                        reject()
                    }
                } else {
                    this.connection = conn
                    resolve()
                }
            })
        })
    }

    async findUserId(phoneNumber) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "SELECT USER_ID FROM USERS WHERE PHONE_NUMBER = (?) ORDER BY CREATED_AT_TIMESTAMP DESC FETCH FIRST 1 ROWS ONLY"
            const bindParams = [phoneNumber];
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('fetch user error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async getUploadToken(uid) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "SELECT UPLOAD_KEY, PHONE_NUMBER FROM USERS WHERE USER_ID = (?) ORDER BY CREATED_AT_TIMESTAMP DESC FETCH FIRST 1 ROWS ONLY"
            const bindParams = [uid];
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('fetch token error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async offboardUser(phoneNumber) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "UPDATE USERS SET PHONE_NUMBER = NULL WHERE PHONE_NUMBER = (?)"
            const bindParams = [phoneNumber]
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('offboard user error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async findUserNumber(phoneNumber) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "SELECT PHONE_NUMBER, USER_ID FROM USERS WHERE PHONE_NUMBER=(?)"
            const bindParams = [phoneNumber]
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('offboard user error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async updateTransferredCdm(boolean, centralId, peripheralId) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "UPDATE CONTACT_COMMUNICATIONS SET TRANSFERRED_TO_CDM = (?) WHERE CENTRAL_USER_ID = (?) AND PERIPHERAL_USER_ID = (?)"
            const bindParams = [boolean, centralId, peripheralId]
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('updating error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async insertCdm(boolean, centralId, peripheralId) {
        console.log(boolean)
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "INSERT INTO CONTACT_COMMUNICATIONS (TRANSFERRED_TO_CDM,CENTRAL_USER_ID,PERIPHERAL_USER_ID) VALUES(?,?,?)"
            const bindParams = [boolean, centralId, peripheralId]
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('insert contact_communications error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async findCommunicatonRecord(centralId, peripheralId) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = "SELECT CENTRAL_USER_ID,PERIPHERAL_USER_ID FROM CONTACT_COMMUNICATIONS WHERE CENTRAL_USER_ID =(?) AND PERIPHERAL_USER_ID =(?) "
            const bindParams = [centralId, peripheralId]
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('cannot search for record', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }

    async searchEncounter(phoneNumber, firstEncounter, lastEncounter, duration) {
        await this.establishConnection()
        return new Promise((resolve, reject) => {
            const queryString = `
                SELECT
                    CD.CENTRAL_USER_ID, CD.PERIPHERAL_USER_ID,
                    UP.PHONE_NUMBER AS CLOSE_CONTACT,
                    DATE(CD.RECORD_TIMESTAMP) AS CONTACT_DATE,
                    COUNT(CD.ID) AS TOTAL_ENCOUNTERS,
                    MAX(CD.RECORD_TIMESTAMP) AS LAST_ENCOUNTER,
                    MIN(CD.RECORD_TIMESTAMP) AS FIRST_ENCOUNTER,
                    TIMESTAMPDIFF(2, char(max(cd.record_timestamp)-min(cd.record_timestamp))) AS CONTACT_TIME_SEC,
                    CC.TRANSFERRED_TO_CDM
                FROM CONTACT_DATA CD, USERS CU, USERS UP
                    LEFT OUTER JOIN CONTACT_COMMUNICATIONS CC
                    ON CD.CENTRAL_USER_ID=CC.CENTRAL_USER_ID
                    AND CD.PERIPHERAL_USER_ID=CC.PERIPHERAL_USER_ID
                WHERE CD.CENTRAL_USER_ID=CU.USER_ID
                    AND CD.PERIPHERAL_USER_ID=UP.USER_ID
                    AND CU.PHONE_NUMBER=(?)
                    AND DATE(CD.RECORD_TIMESTAMP) BETWEEN (?) AND (?)
                    AND UP.PHONE_NUMBER IS NOT NULL
                    AND CD.RSSI >= -75
                GROUP BY CD.CENTRAL_USER_ID, CD.PERIPHERAL_USER_ID,
                    UP.PHONE_NUMBER, CC.TRANSFERRED_TO_CDM,
                    DATE(CD.RECORD_TIMESTAMP)
                    HAVING TIMESTAMPDIFF(2, char(max(cd.record_timestamp)-min(cd.record_timestamp))) > (?)`
            const bindParams = [phoneNumber, firstEncounter, lastEncounter, duration]
            this.connection.query(queryString, bindParams,
                (err, data) => {
                    if (err) {
                        console.error('contact encounter error', err)
                        this.connection.closeSync()
                        reject(err)
                    } else {
                        this.connection.closeSync()
                        resolve(data)
                    }
                }
            )
        })
    }
}
module.exports.DatabaseInstance = DatabaseInstance
