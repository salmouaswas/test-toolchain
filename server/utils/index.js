const moment = require('moment');

class CustomError extends Error {
  constructor(code = 'GENERIC', status = 500, ...params) {
      super(...params)

      if (Error.captureStackTrace) {
          Error.captureStackTrace(this, CustomError)
      }

      this.code = code
      this.status = status
  }
}

const currentTimeStamp = () => {
  return moment(Date.now()).format("YYYY-MM-DD HH:mm:ss");
}

const processSearchResultsForLog = (logs) => {
  const logsArray = logs.map(entry => `${entry.PERIPHERAL_USER_ID}`);
  const consoleLogResult = `> ${logsArray.join(', ')} <`
  return consoleLogResult;
}

module.exports = {
  CustomError,
  currentTimeStamp,
  processSearchResultsForLog
};