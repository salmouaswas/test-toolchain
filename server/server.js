/* eslint-disable no-console */
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const path = require("path");
const cors = require("cors");
const request = require("request");
require("dotenv").config();
const {
  getUserFromNumber,
  offboardUserFromNumber,
  findUser,
  updateRegisteredCMD,
  authIdentity,
  searchEncounter,
  getUploadToken
} = require("./controllers");
const { currentTimeStamp, processSearchResultsForLog } = require('./utils');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

let appIdService = {};

if (process.env.APPID_SECRETS)
  appIdService = JSON.parse(process.env.APPID_SECRETS);
else console.log("ERROR:  Cannot find AppID service secrets!");

if (process.env.NODE_ENV === "production") {
  app.get("*", (req, res, next) => {
    // retrieve the authorization header applied by the AppID service
    // authorization header in the form 'Bearer <access token> <id token>'
    const header = req.header("authorization");
    const tokens = header ? header.split(" ") : ["1", "2"];

    // token[0] = 'Bearer'
    // token[1] = access token
    // token[2] = identity token
    // usually only need the access token
    const authorization = `${tokens[0]} ${tokens[1]}`;
    const headers = {
      Authorization: authorization,
      accept: "application/json",
    };    

    // prepare call to AppID service
    const options = { url: `${appIdService.oauthServerUrl}/userinfo`, headers };

    // call the AppID service to retrieve user information
    request(options, (error) => {
      if (error) {
        console.log("route error", req.originalUrl, req.baseUrl, req.path);
        console.log(error);
        res.send(error);
        return;
      }

      // Request was successful, continue
      next();
    });

    // Log user accessing site.
    const user = authIdentity(appIdService, req);
    if (!user.includes('Not Found')) {
      console.log(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} is accessing site `);
    }
  });
}

// Set up React First
if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "build")));
}
app.use(cors());
app.get("/getUser/:phoneNumber", async (req, res) => {
  try {
    const uid = await getUserFromNumber(req.params.phoneNumber);

    console.log(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} is requesting user ${uid.length ? uid[0].USER_ID: 'no user'} for Upload Token `);

    res.status(200).send(uid);
  } catch (e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;
    
    console.error(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} 
    is requesting an Upload Token for a user which does not exist and Failed - ${errorStatus} - ${errorCode}`);

    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
});
app.get("/triggerUpload/:uid", async (req, res) => {
  try {
    await getUploadToken(req.params.uid);

    console.log(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} is requesting user ${req.params.uid} for Upload Token `);

    res.status(200).send('success');
  } catch (e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;
    
    console.error(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} 
    is triggering an Upload Token for a user which does not exist and Failed - ${errorStatus} - ${errorCode}`);

    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
});
app.get("/offboard/:phoneNumber", async (req, res) => {
  try {
    const uid = await getUserFromNumber(req.params.phoneNumber);
    const number = await offboardUserFromNumber(req.params.phoneNumber);
    console.log(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} is offboarding user ${uid.length > 0 ? uid[0].USER_ID : 'that does not exist'} `);
    res.status(200).send(number);
  } catch (e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;

    console.error(`${currentTimeStamp()} - Request to offboard user that does not exist 
    by ${authIdentity(appIdService, req)} Failed - ${errorStatus} - ${errorCode}`);

    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
});
app.get("/getPhoneNumber/:phoneNumber", async (req, res) => {
  try {
    const user = await findUser(req.params.phoneNumber)
    
    console.log(`${currentTimeStamp()} - User ${authIdentity(appIdService, req)} 
    is checking whether they can offboard user ${user.length > 0 ? user[0].USER_ID : 'that does not exist'}  `);

    res.status(200).send(user)
  } catch (e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;

    console.error(`${currentTimeStamp()} - Checking whether they can offboard user that does not exist 
    by ${authIdentity(appIdService, req)} Failed - ${errorStatus} - ${errorCode}`);

    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
})
app.get("/updateCDMStatus/:boolean/:centralId/:peripheralId", async (req, res) => {
  try {
    const change = await updateRegisteredCMD(req.params.boolean, req.params.centralId, req.params.peripheralId)

    console.log(`${currentTimeStamp()} 
    - User ${authIdentity(appIdService, req)} 
    is updating peripheral user ${req.params.peripheralId} status of being registered in CDOM 
    related to central user ${req.params.centralId} to ${req.params.boolean}`);

    res.status(200).send({change})
  } catch (e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;

    console.error(`${currentTimeStamp()} - Update CDOM registered Status of ${req.params.peripheralId} 
    by ${authIdentity(appIdService, req)} to ${req.params.boolean} Failed - ${errorStatus} - ${errorCode}`);

    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
})
app.get("/search/:phoneNumber/:firstEncounter/:lastEncounter/:duration", async (req, res) => {
  let encounter;
  let uid = [];
  try {
    uid = await getUserFromNumber(req.params.phoneNumber);
  } catch (e) {
    console.error(`Unable to retrieve central user`); 
  }
  try {
    encounter = await searchEncounter(req.params.phoneNumber, req.params.firstEncounter, req.params.lastEncounter, req.params.duration)
    const peripheralUsers = processSearchResultsForLog(encounter);

    console.log(`${currentTimeStamp()} 
    - User ${authIdentity(appIdService, req)} 
    searched for central user: ${uid.length > 0 ? uid[0].USER_ID : 'that does not exist'} with parameters 
    - First Encounter: ${req.params.firstEncounter} 
    - Last Encounter: ${req.params.lastEncounter} 
    - Duration: ${req.params.duration} min
    - and returned peripheral users ${peripheralUsers}`);

    res.status(200).send(encounter)
  } catch (e) {
    const errorCode = e.code || 'Something went wrong';
    const errorStatus = e.status || 500;

    console.error(`${currentTimeStamp()} 
    - User ${authIdentity(appIdService, req)} 
    searched for central user: ${uid.length > 0 ? uid[0].USER_ID : 'that does not exist'} with parameters 
    - First Encounter: ${req.params.firstEncounter} 
    - Last Encounter: ${req.params.lastEncounter} 
    - Duration: ${req.params.duration} min
    - and request failed with - ${errorStatus} - ${errorCode}`);

    if (e.stack) console.error(e.stack);
    res.status(errorStatus).send(errorCode);
  }
})

// React to catch all other routes. HAS TO BE LAST
if (process.env.NODE_ENV === "production") {
  app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html"));
  });
}

const port = process.env.PORT || 5000;
app.listen(port);

// eslint-disable-next-line no-console
console.log(`App is listening on port ${port}`);

module.exports = app;