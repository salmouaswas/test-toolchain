/* eslint-disable no-console */
const moment = require('moment');
const jwtDecode = require('jwt-decode');
const { CustomError } = require('../utils');
const { DatabaseInstance } = require('../services/Database');
const { sendMessage } = require('../services/Twillio');

const getAuthIdentity = (appIdService, req) => {
    const header = req.header("authorization");
    const userId = {
        email: 'Not Found',
        ids: 'No Provider Ids Found'
    };
    if (header) {
        const tokens = header.split(" ");

        tokens.forEach((token, i) => {
            if (i !== 0) {
                try {
                    const decoded = jwtDecode(token);
                    if (decoded.email) {
                        userId.email = decoded.email;
                    }
                    if (decoded.identities && decoded.identities.length > 0) {
                        const ids = [];
                        decoded.identities.forEach(id => {
                            ids.push(`Provider: ${id.provider} - id: ${id.id}`);
                        })
                        userId.ids = `ProviderIDs > ${ids.join(', ')} <`;
                    }
                } catch (e) {/* Token Provided is invalid jwt */ }
            }

        })
    }

    return `> Email: ${userId.email} ProviderID: ${userId.ids} <`;
}

module.exports = {
    authIdentity: (appIdService, req) => {
      return getAuthIdentity(appIdService, req);
    },
    getUserFromNumber: async (phoneNumber) => {
      let database;
      let userid;

      try {
          database = new DatabaseInstance()
      } catch (e) {
          throw new CustomError('Unable to connect to db2', 500);
      }

      try {
        userid = await database.findUserId(phoneNumber)
      } catch (e) {
        throw new CustomError('Unable to get user id', 500);
      }
  
      return userid
    },
    getUploadToken: async (uid) => {
      let database;
      let user;

      try {
          database = new DatabaseInstance()
      } catch (e) {
          throw new CustomError('Unable to connect to db2', 500);
      }

      try {
        user = await database.getUploadToken(uid)
      } catch (e) {
        throw new CustomError('Unable to get user token', 500);
      }

      if(user.length && user[0].UPLOAD_KEY) {
        try {
          await sendMessage(user[0].PHONE_NUMBER, user[0].UPLOAD_KEY);
        } catch (e) {
          throw new CustomError('Unable to send upload token', 500);
        }
      } else {
        if (!user[0].UPLOAD_KEY) {
          throw new CustomError('Token does not exist', 404);
        }
        throw new CustomError('User does not exist', 500);
      }
    },
    offboardUserFromNumber: async (phoneNumber) => {
      let database;
      let newPhone;
  
      try {
        database = new DatabaseInstance()
      } catch (e) {
        throw new CustomError('Unable to connect to db2', 500);
      }

      try {
        newPhone = await database.offboardUser(phoneNumber)
      } catch (e) {
        throw new CustomError('Unable to get offboarded user', 500);
      }
  
      return newPhone
    },
    findUser: async (phoneNumber) => {
      let database;
      let phone;
  
      try {
        database = new DatabaseInstance()
      } catch (e) {
        throw new CustomError('Unable to connect to db2', 500);
      }

      try {
        phone = await database.findUserNumber(phoneNumber)
      } catch (e) {
        throw new CustomError('Unable to get phone number', 500);
      }

      return phone
    },
    updateRegisteredCMD: async (boolean, centralId, peripheralId) => {
      let database;
      let record;
  
      try {
        database = new DatabaseInstance()
      } catch (e) {
        throw new CustomError('Unable to connect to db2', 500);
      }

      try {
        record = await database.findCommunicatonRecord(centralId, peripheralId)
      } catch (e) {
        throw new CustomError('Unable to get comm record', 500);
      }
  
      if (record.length === 0) {
        try {
          await database.insertCdm(boolean, centralId, peripheralId)
        } catch (e) {
          throw new CustomError('Unable to insert', 500);
        }
        return boolean
      }

      try {
        await database.updateTransferredCdm(boolean, centralId, peripheralId)
      } catch (e) {
        throw new CustomError('Unable to get update transferred cdm', 500);
      }
    
      return boolean
    },
    searchEncounter: async (phoneNumber, firstEncounter, lastEncounter, duration) => {
        let encounters;
        let database;

        // converting from Sat Jun 1 12:00:00 to YYYY-MM-DDD as required by database
        const finalFirstDate = moment(new Date(firstEncounter)).format('YYYY-MM-DD');
        const finalLastDate = moment(new Date(lastEncounter)).format('YYYY-MM-DD');

        // converting from minutes to seconds
        const seconds = duration * 60 - 1;
        try {
            database = new DatabaseInstance()
        } catch (e) {
            throw new CustomError('Unable to connect to db2', 500);
        }
        try {
            encounters = await database.searchEncounter(phoneNumber, finalFirstDate, finalLastDate, seconds)
        } catch (e) {
            throw new CustomError('Unable to get encounters', 500);
        }
        return encounters;
    }
}
