module.exports = {
  moduleFileExtensions: ['jsx', 'js'],
  transform: {
    '^.+\\.jsx$': 'babel-jest',
    '^.+\\.js$': 'babel-jest',
  },
  globals: {
    'babel-jest': {
      useBabelrc: true,
    },
  },
  coveragePathIgnorePatterns: ['/node_modules/', 'enzyme.js'],
  testPathIgnorePatterns: ['/test-automation/tests/'],
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  setupFiles: ['<rootDir>/jest.setup.js'],
  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js'
  },
};