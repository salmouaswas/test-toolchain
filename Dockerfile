FROM node:12

RUN mkdir /app
WORKDIR /app

COPY package*.json /app/
RUN npm install --only=prod
RUN apt-get update && apt-get install -y git

COPY public /app/public
COPY server /app/server
COPY src /app/src

ENV NODE_ENV production
ENV PORT 5000

EXPOSE 5000

CMD ["npm", "run", "prod"]
